using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICombat
{
    void Hurt(BaseUnit objective);
    void Dead(BaseUnit objective);
}
