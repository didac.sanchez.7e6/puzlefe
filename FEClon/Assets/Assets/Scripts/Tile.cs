using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Tile : MonoBehaviour
{
    [SerializeField] private GameObject highligth;
    [SerializeField] private GameObject selctColor;
    [SerializeField] private bool isWalkable;
    public BaseUnit occupiedUnit;
    public bool selectabele = false;
    public bool visitet = false;
    public Tile parent;
    public int distancia = 0;

    //AI
    public float distanciaToObjective;


    public bool Walkeable => isWalkable && occupiedUnit == null;
    void OnMouseEnter()
    {
        highligth.SetActive(true);
        MenuManager.Instance.ShowEnemyInfo(this.occupiedUnit);
        if (occupiedUnit != null && UnitsManager.Instance.selectPlayer == null)
        {
            if (occupiedUnit.Faction == Faction.Enemy)
            {
                GridManager.Instance.ResteListSelect();
                FindSelectableTiles();
            }
        }
    }
    void OnMouseExit()
    {
        highligth.SetActive(false);
        if (occupiedUnit != null && UnitsManager.Instance.selectPlayer == null)
        {
            if (occupiedUnit.Faction == Faction.Enemy)
            {
                MenuManager.Instance.ShowEnemyInfo(null);
                GridManager.Instance.ResteListSelect();
            }
        }
    }
    private void OnMouseDown()
    {
        if (GameManager.Instance.state != GameManager.GameState.TurnPleyer) return;

        if (occupiedUnit != null)
        {
            if (occupiedUnit.Faction == Faction.Player)
            {
                if (occupiedUnit.canMove && UnitsManager.Instance.selectPlayer == null)
                {
                    UnitsManager.Instance.SetSelectHero((BasePlayer)occupiedUnit);
                    FindSelectableTiles();
                }
            }
            else
            {
                if (UnitsManager.Instance.selectPlayer != null)
                {
                    var enemy = (BaseEnemy)occupiedUnit;
                    //Atack
                    //Esto solo es por testeo
                    UnitsManager.Instance.selectPlayer.Hurt(enemy);
                    UnitsManager.Instance.selectPlayer.counterattack = true;
                    enemy.counterattack = true;
                    UnitsManager.Instance.SetSelectHero(null);
                    occupiedUnit.canMove = false;
                    UnitsManager.Instance.CheckTurnEnd();
                }
            }
        }
        else
        {
            if (UnitsManager.Instance.selectPlayer != null && selectabele)
            {
                SetUnit(UnitsManager.Instance.selectPlayer);
                UnitsManager.Instance.SetSelectHero(null);
                occupiedUnit.canMove = false;
                UnitsManager.Instance.CheckTurnEnd();
            }
        }
    }

    public void FindSelectableTiles()
    {
        Queue<Tile> proses = new();
        proses.Enqueue(occupiedUnit.occupiedTile);
        occupiedUnit.occupiedTile.visitet = true;

        while (proses.Count > 0)
        {
            Tile tile = proses.Dequeue();
            tile.selectabele = true;
            GridManager.Instance.selectablesTiles.Add(tile);
            if (tile.distancia < occupiedUnit.move)
            {
                List<Tile> list = FindAdjentTiles(tile);
                foreach (Tile t in list)
                {
                    if (!t.visitet)
                    {
                        t.parent = tile;
                        t.visitet = true;
                        t.distancia = 1 + tile.distancia;
                        proses.Enqueue(t);
                        t.selctColor.SetActive(true);
                    }
                }
            }
        }
    }

    public void RestTile()
    {
        parent = null;
        visitet = false;
        distancia = 0;
        selctColor.SetActive(false);
        selectabele = false;
    }

    public List<Tile> FindAdjentTiles(Tile t)
    {
        Tile topTile, bottomTile, leftTile, rightTile;
        List<Tile> tiles = new();
        GameObject top = GameObject.Find("Tile_" + Convert.ToString(Convert.ToInt64(t.name.Split("_")[1]) + 1) + "_" + t.name.Split("_")[2]);
        if (top != null)
        {
            topTile = top.GetComponent<Tile>();
            tiles.Add(topTile);
        }
        GameObject bottom = GameObject.Find("Tile_" + Convert.ToString(Convert.ToInt64(t.name.Split("_")[1]) - 1) + "_" + t.name.Split("_")[2]);
        if (bottom != null)
        {
            bottomTile = bottom.GetComponent<Tile>();
            tiles.Add(bottomTile);
        }
        GameObject left = GameObject.Find("Tile_" + t.name.Split("_")[1] + "_" + Convert.ToString(Convert.ToInt64(t.name.Split("_")[2]) - 1));
        if (left != null)
        {
            leftTile = left.GetComponent<Tile>();
            tiles.Add(leftTile);
        }
        GameObject right = GameObject.Find("Tile_" + t.name.Split("_")[1] + "_" + Convert.ToString(Convert.ToInt64(t.name.Split("_")[2]) + 1));
        if (right != null)
        {
            rightTile = right.GetComponent<Tile>();
            tiles.Add(rightTile);
        }
        return tiles;
    }

    public void SetUnit(BaseUnit unit)
    {

        if (unit.occupiedTile != null) unit.occupiedTile.occupiedUnit = null;
        unit.transform.position = transform.position;
        occupiedUnit = unit;
        unit.occupiedTile = this;
    }
}
