using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseUnit : MonoBehaviour, ICombat
{
    public Tile occupiedTile;
    public Faction Faction;
    public bool canMove = true;
    public bool canAttack = true;

    public int move = 5;

    public int attack;
    public int maxLife;
    public int actualHealth;
    public bool counterattack = true;

    [SerializeField] FloatingHealthBar healthBar;

    public string nameUnit;
    private void Awake()
    {
        healthBar = GetComponentInChildren<FloatingHealthBar>();
        actualHealth = maxLife;
    }

    public void Hurt(BaseUnit objective)
    {
        if (canAttack )
        {
            if (counterattack)
            {
                canAttack = false;
            }

            
            Debug.Log("Pain");
            objective.actualHealth = Mathf.Max(objective.actualHealth - attack, 0);
            objective.healthBar.UpdateHealthBar(objective);
            if (objective.actualHealth == 0)
            {
                Dead(objective);
            }
            if (objective.counterattack && objective.actualHealth > 0)
            {
                objective.counterattack = false;
                objective.Hurt(this);
            }
        }
        counterattack = true;
    }

    public void Dead(BaseUnit objective)
    {
        if(objective.Faction == Faction.Player)
        {
            UnitsManager.Instance.player.Remove((BasePlayer)objective);
        }
        else UnitsManager.Instance.enemys.Remove((BaseEnemy)objective);
        Destroy(objective.gameObject);
        objective = null;
    }
}
