using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using static GameManager;
using static UnityEngine.EventSystems.EventTrigger;

public class EnemyMove : MonoBehaviour
{
    public BasePlayer target = null;
    public static EnemyMove Instance;
    public Tile nodeG = null;

    private void Awake()
    {
        Instance = this;
    }

    public void MoveEnemy()
    {
        if (UnitsManager.Instance.enemys.Count >= 1)
        {
            foreach (BaseEnemy enemy in UnitsManager.Instance.enemys)
            {
                if (enemy.canMove)
                {
                    nodeG = null;
                    GridManager.Instance.ResteListSelect();
                    enemy.canMove = false;
                    enemy.canAttack = true;
                    CalcPath(enemy.occupiedTile);
                }
            }
            UnitsManager.Instance.EndTurnEnemy();
            GameManager.Instance.ChangeStates(GameState.TurnPleyer);
        }
    }

    private void CalcPath(Tile enemy)
    {
        FindTrget(enemy);
        FindPath(enemy);
    }

    private void FindPath(Tile enemy)
    {
        enemy.FindSelectableTiles();
        FindSelectableNode(enemy);
    }

    private void FindTrget(Tile enemy)
    {
        BasePlayer near = null;
        float distance = Mathf.Infinity;

        foreach (BasePlayer tar in UnitsManager.Instance.player)
        {
            float dista = Vector3.Distance(enemy.gameObject.transform.position, tar.gameObject.transform.position);
            if (dista < distance)
            {
                distance = dista;
                near = tar;
            }
        }
        target = near;
    }

    private void FindSelectableNode(Tile enemy)
    {
        float minDistancie = Mathf.Infinity;
        foreach(Tile t in GridManager.Instance.selectablesTiles)
        {
            if (Vector3.Distance(t.gameObject.transform.position, target.gameObject.transform.position) < minDistancie)
            {
                minDistancie = Vector3.Distance(t.gameObject.transform.position, target.gameObject.transform.position);
                nodeG = t;
            }
        }
        Win(nodeG, enemy);
    }

    private static Queue<Tile> Order(Queue<Tile> proses)
    {
        IEnumerable<Tile> query = proses.OrderBy(node => node.distancia);
        Queue<Tile> prosesOrdn = new();
        foreach (Tile node in query)
        {
            prosesOrdn.Enqueue(node);
        }

        return prosesOrdn;

    }
    private void Win(Tile node, Tile enemy)
    {
        bool move = CheckMuve(node, enemy), attack, checkAgain = true;
        if (move)
        {
            node.SetUnit(enemy.occupiedUnit);
            foreach (var t in GridManager.Instance.selectablesTiles)
            {
                t.RestTile();
            }
            checkAgain = false;

        }
        if (!move && checkAgain) move = CheckMuve(node.parent, enemy);
        if (move && checkAgain)
        {
            node = node.parent;
            node.SetUnit(enemy.occupiedUnit);
            foreach (var t in GridManager.Instance.selectablesTiles)
            {
                t.RestTile();
            }

        }
        else
        {
            return;
        }
        attack = CheckAttack(node);
        if (attack)
        {            
            node.occupiedUnit.Hurt(target);
        }

    }
    private bool CheckMuve(Tile node, Tile enemy)
    {
        Debug.Log(node.name);
        return (node.selectabele && node.occupiedUnit == null) || node.occupiedUnit == enemy.occupiedUnit;
    }
    private bool CheckAttack(Tile node)
    {
        bool attack = false;
        foreach (Tile t in node.FindAdjentTiles(node))
        {
            if (t.gameObject.transform.position.Equals(target.gameObject.transform.position))
            {
                attack = true;
            }
        }
        return attack;
    }
}
