﻿using UnityEngine;
using UnityEngine.UI;
public class FloatingHealthBar : MonoBehaviour
{
    [SerializeField] private Slider slider;

    public void UpdateHealthBar(BaseUnit unit)
    {
        Debug.Log(unit.actualHealth / (float)unit.maxLife +" " +unit.name);
        slider.value = unit.actualHealth / (float)unit.maxLife;
    }
}
