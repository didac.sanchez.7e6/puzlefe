using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;
using static GameManager;

public class UnitsManager : MonoBehaviour
{
    public static UnitsManager Instance;
    private List<ScriptableUnit> units;
    public BasePlayer selectPlayer;
    public List<BasePlayer> player = new();
    public List<BaseEnemy> enemys = new();
    private bool endTrun = false, previus = false;
    private void Awake()
    {
        Instance = this;
        units = Resources.LoadAll<ScriptableUnit>("Units").ToList();
    }

    public void SpawnPlayer()
    {
        // Esto esta en testing
        int playerCount = 2;

        for (int i = 0; i < playerCount; i++)
        {
            BasePlayer player = GetRandomUnit<BasePlayer>(Faction.Player);
            var spawnedPlayer = Instantiate(player);
            var randomTile = GridManager.Instance.GetSpawnPlayer();

            randomTile.SetUnit(spawnedPlayer);
            this.player.Add(spawnedPlayer);
        }

        GameManager.Instance.ChangeStates(GameState.SpawnEnemy);
    }
    public void SpawnEnemy()
    {
        // Esto esta en testing
        int enemyCount = 2;

        for (int i = 0; i < enemyCount; i++)
        {
            BaseEnemy enemy = GetRandomUnit<BaseEnemy>(Faction.Enemy);
            var spawnedEnemy = Instantiate(enemy);
            var randomTile = GridManager.Instance.GetSpawnEnemy();

            enemys.Add(spawnedEnemy);

            randomTile.SetUnit(spawnedEnemy);
        }

        GameManager.Instance.ChangeStates(GameState.TurnPleyer);
    }

    private T GetRandomUnit<T>(Faction faction) where T : BaseUnit
    {
        return (T)units.Where(u => u.Faction == faction).OrderBy(o => Random.value).First().UnitPrefap;
    }

    public void SetSelectHero(BasePlayer player)
    {
        selectPlayer = player;
        MenuManager.Instance.ShowSelectHero(selectPlayer);
    }
    public void CheckTurnEnd()
    {
        previus = false;
        endTrun = false;
        foreach (var unit in player)
        {
            if (unit.canMove) previus = true;
        }
        if (!previus) endTrun = true;
        GridManager.Instance.ResteListSelect();
        if (endTrun)
        {
            GameManager.Instance.ChangeStates(GameState.TurnEnemy);
        }
        if (!endTrun)
        {
            GameManager.Instance.ChangeStates(GameState.TurnPleyer);
        }
    }

    public void EndTurnEnemy()
    {
        foreach (BasePlayer unit in player)
        {
            unit.canMove = true;
            unit.canAttack = true;
            unit.counterattack = true;
        }
        foreach (BaseEnemy unit in enemys)
        {
            unit.canMove = true;
            unit.canAttack = true;
            unit.counterattack = true;
        }
    }
}
