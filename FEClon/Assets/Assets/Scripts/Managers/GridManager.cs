using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Tilemaps;

public class GridManager : MonoBehaviour
{
    public static GridManager Instance;
    public int width, height;
    public Tile tilePrefap;
    [SerializeField]
    private Transform _cam;
    public Dictionary<Vector2,Tile> tiles = new();
    public List<Tile> selectablesTiles = new();
    private void Awake()
    {
        Instance = this;
    }
    public void CreateGrid()
    {
        for (int y = 0; y < width; y++)
        {

            for(int x = 0; x < height; x++)
            {

                var spawnTile = Instantiate(tilePrefap, new Vector2(y -0.5f,x -0.5f), Quaternion.identity);
                spawnTile.name = $"Tile_{x}_{y}";

                tiles[new Vector2(y, x)] = spawnTile;
            }

        }

        _cam.position = new Vector3((float)width / 2 -1f, (float)height / 2 -1f, -10f);

        GameManager.Instance.ChangeStates(GameManager.GameState.SpawnPleyer);
    }

    //No Definitivo 
    public Tile GetSpawnPlayer()
    {
        return tiles.Where(t => t.Key.x < width/2 && t.Value.Walkeable).OrderBy(t => Random.value).First().Value;
    }
    public Tile GetSpawnEnemy()
    {
        return tiles.Where(t => t.Key.x > width / 2 && t.Value.Walkeable).OrderBy(t => Random.value).First().Value;
    }
    public void ResteListSelect()
    {
        foreach (Tile t in selectablesTiles)
        {
            t.RestTile();
        }
        selectablesTiles.Clear();
    }
}
