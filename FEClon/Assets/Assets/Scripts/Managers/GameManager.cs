using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public enum GameState { GenerarGrid = 0, SpawnPleyer = 1, SpawnEnemy = 2, TurnPleyer = 3, TurnEnemy = 4 };
    public GameState state = 0;
    public static GameManager Instance;
    private void Awake()
    {
        Instance = this;
    }
    private void Start()
    {
        ChangeStates(GameState.GenerarGrid);
    }
    public void ChangeStates(GameState newState)
    {
        state = newState;
        switch (state)
        {
            case GameState.GenerarGrid:
                GridManager.Instance.CreateGrid();
                break;
            case GameState.SpawnPleyer:
                UnitsManager.Instance.SpawnPlayer();
                break;
            case GameState.SpawnEnemy:
                UnitsManager.Instance.SpawnEnemy();
                break;
            case GameState.TurnPleyer:
                break;
            case GameState.TurnEnemy:
                Debug.Log("Enemy Turn");
                EnemyMove.Instance.MoveEnemy();
                break;

        }
    }
}
