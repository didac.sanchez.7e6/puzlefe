using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
    public static MenuManager Instance;

    [SerializeField] private GameObject _selectPlayer, _selectEnemy;

    private void Awake()
    {
        Instance = this;
    }

    public void ShowEnemyInfo(BaseUnit tile)
    {
        if (tile != null && tile.Faction == Faction.Enemy)
        {
            _selectEnemy.GetComponentInChildren<TMP_Text>().text = tile.nameUnit;
            _selectEnemy.SetActive(true);
            return;
        }
        if (tile == null)
        {
            _selectEnemy.SetActive(false);
        }
    }

    public void ShowSelectHero(BasePlayer player)
    {
        if (player == null)
        {
            _selectPlayer.SetActive(false);
            return;
        }
        _selectPlayer.GetComponentInChildren<TMP_Text>().text = player.nameUnit;
        _selectPlayer.SetActive(true);
    }
}
